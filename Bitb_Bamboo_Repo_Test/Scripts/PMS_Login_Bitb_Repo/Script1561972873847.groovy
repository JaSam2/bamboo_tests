import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://www.seznam.cz')

WebUI.delay(2)

WebUI.navigateToUrl('http://10.218.29.5:8500/pmsV3app/#/login') //http://127.0.0.1.5:8500/pmsV3app/#/login

WebUI.verifyElementPresent(findTestObject('Object Repository/Login Page Header'), 2)

//WebUI.verifyElementPresent(findTestObject('Object Repository/TTTT_Objects/Password field'), 2)

//WebUI.setText(findTestObject('Object Repository/TTTT_Objects/Username Field'), 'balsik', FailureHandling.STOP_ON_FAILURE)

//WebUI.setText(findTestObject('Object Repository/TTTT_Objects/Password field'), '12JaSam68801Jde', FailureHandling.STOP_ON_FAILURE)

//WebUI.verifyElementPresent(findTestObject('Object Repository/TTTT_Objects/Login button'), 2)

//WebUI.verifyElementText(findTestObject('Object Repository/TTTT_Objects/Login button'), 'Login', FailureHandling.STOP_ON_FAILURE)

//WebUI.click(findTestObject('Object Repository/TTTT_Objects/Login button'))

//WebUI.delay(15) //delay shorter than 15 sec causes test fail as the the element (Avatar) for subsequent action is somehow unclickable...

//WebUI.verifyElementPresent(findTestObject('Object Repository/TTTT_Objects/PMS_LogOut_Avatar_Img(optional)'), 2)

//WebUI.verifyElementClickable(findTestObject('Object Repository/TTTT_Objects/PMS_LogOut_Avatar_Img(optional)'), FailureHandling.STOP_ON_FAILURE)

//WebUI.click(findTestObject('Object Repository/TTTT_Objects/PMS_LogOut_Avatar_Img(optional)'))

//WebUI.delay(10)

//WebUI.verifyElementPresent(findTestObject('Object Repository/TTTT_Objects/LogOut_Link'), 1)

//WebUI.click(findTestObject('Object Repository/TTTT_Objects/LogOut_Link'))

WebUI.delay(5)

WebUI.closeBrowser()